import { useState, useEffect } from "react";

function useFormValidation(initialState, validate, authenticate) {
    const [values, setValues] = useState(initialState)
    const [errors, setErrors] = useState({})
    const [isSubmitting, setSubmitting] = useState(false)

    useEffect(() => {

        if (isSubmitting) {
            const noErrors = Object.keys(errors).length === 0
            if (noErrors) {
                authenticate()
                setSubmitting(false)
            } else {
                console.log('Error: ', errors)
                setSubmitting(false)
            }
        }
    }, [errors])

    const handleChange = (e) => {
        e.persist()
        setValues(previousValue => ({
            ...previousValue,
            [e.target.name]: e.target.value
        }))
    }

    const handleBlur = () => {
        const validationErrors = validate(values)
        setErrors(validationErrors)
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        const validationErrors = validate(values)
        setErrors(validationErrors)
        setSubmitting(true)
    }
    return { handleSubmit, handleBlur, handleChange, values, errors, isSubmitting }
}

export default useFormValidation;
