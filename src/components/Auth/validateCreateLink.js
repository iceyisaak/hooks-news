export default function validateCreateLink(values) {
    let errors = {}

    // Description Errors
    if(!values.description){
        errors.description = "Description Required"
    }else if (values.description.length < 10 ){
        errors.description = "Description must be at leaset 10 Characters"
    }
    // URL Errors
    if(!values.url){
        errors.url = "URL Required"
    }else if(!/^(ftp|http|https):\/\/[^ "]+$/.test(values.url)){
        errors.url = "URL must be valid"
    }

    return errors
}
