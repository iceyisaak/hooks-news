import React, { useState } from "react";
import useFormValidation from "./useFormValidation"
import validateLogin from "./validateLogin";
import firebase from '../../firebase'
import { Link } from 'react-router-dom'

const INITIAL_STATE = {
  name: "",
  email: "",
  password: ""
}

function Login(props) {
  const {
    handleSubmit,
    handleBlur,
    handleChange,
    values,
    errors,
    isSubmitting } = useFormValidation(INITIAL_STATE, validateLogin, authenticateUser)

  const [login, setLogin] = useState(true)
  const [firebaseError, setFirebaseError] = useState(null)

  async function authenticateUser() {

    const { name, email, password } = values

    try {
      login
        ? await firebase.login(email, password)
        : await firebase.register(name, email, password)
      props.history.push("/")

    } catch (err) {
      console.error('Authentication Error', err)
      setFirebaseError(err.message)
    }
  }

  return (
    <div>
      <h2 className="mv3">{login ? "Login" : "Create an Account"}</h2>

      <form onSubmit={handleSubmit} className="flex flex-column">

        {!login && <input
          type="text"
          name="name"
          value={values.name}
          onChange={handleChange}
          placeholder="Your Name"
          autoComplete="off" />}

        <input
          type="email"
          name="email"
          className={errors.email && 'error-input'}
          value={values.email}
          onChange={handleChange}
          onBlur={handleBlur}
          placeholder="Your Email"
          autoComplete="off" />
        {errors.email && <p className="error-text">{errors.email}</p>}

        <input
          type="password"
          name="password"
          className={errors.password && 'error-input'}
          value={values.password}
          onChange={handleChange}
          onBlur={handleBlur}
          placeholder="Choose a Secure Password" />
        {errors.password && <p className="error-text">{errors.password}</p>}
        {firebaseError && <p className="error-text">{firebaseError}</p>}

        <div className="flex mt3">

          <button type="submit" className="button pointer mr2" disabled={isSubmitting} style={{ background: isSubmitting ? 'grey' : 'orange' }}>
            Submit
          </button>

          <button type="button" className="button pointer" onClick={() => setLogin(prevLogin => !prevLogin)}>
            {login ? "need to create an account?" : "already have an account?"}
          </button>

        </div>

      </form>

      <div className="forgot-password">
        <Link to="/forgot">Forgot Password?</Link>
      </div>
    </div>
  )
}

export default Login;
